package org.elu.learn.spring.boot3.ch02

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping

@Controller
class HelloController(private val videoService: VideoService) {
    @GetMapping("/")
    fun index(model: Model): String {
        model.addAttribute("videos", videoService.getVideos())
        return "index"
    }

    @PostMapping("/new-video")
    fun newVideo(@ModelAttribute newVideo: Video): String {
        videoService.create(newVideo)
        return "redirect:/"
    }
}
