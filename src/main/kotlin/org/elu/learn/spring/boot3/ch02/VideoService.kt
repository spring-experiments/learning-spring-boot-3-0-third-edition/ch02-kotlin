package org.elu.learn.spring.boot3.ch02

import org.springframework.stereotype.Service

@Service
class VideoService {
    private var videos: List<Video> = listOf(
        Video("Need HELP with your SPRING BOOT 3 App?"),
        Video("Don't do THIS to your own CODE!"),
        Video("SECRETS to fix BROKEN CODE!")
    )

    fun getVideos(): List<Video> = videos

    fun create(newVideo: Video): Video {
        this.videos += newVideo
        return newVideo
    }
}
