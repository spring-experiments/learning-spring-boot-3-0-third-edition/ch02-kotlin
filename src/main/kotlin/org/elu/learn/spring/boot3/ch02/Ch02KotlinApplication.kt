package org.elu.learn.spring.boot3.ch02

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Ch02KotlinApplication

fun main(args: Array<String>) {
    runApplication<Ch02KotlinApplication>(*args)
}
