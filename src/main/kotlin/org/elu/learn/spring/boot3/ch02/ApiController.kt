package org.elu.learn.spring.boot3.ch02

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/videos")
class ApiController(private val videoService: VideoService) {
    @GetMapping
    fun all(): List<Video> = videoService.getVideos()

    @PostMapping
    fun newVideo(@RequestBody newVide: Video): Video = videoService.create(newVide)
}
